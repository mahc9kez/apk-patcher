#!/bin/bash

#SMALI=$(grep -rl 'APK is not signed by LatinApp certificates' ./)

cp $(dirname $0)/xml/{keyboard_latin_sangaline.xml,keyboard_latin_sangaline_base.xml,keymapping_sangaline.xml} res/xml

gsed -i -e '/android.permission.GET_ACCOUNTS/d; /android.permission.READ_CONTACTS/d; /android.permission.READ_PROFILE/d; /android.hardware.microphone/d; s/\ android:backupAgent=\"com.google.android.apps.inputmethod.latin.core.LatinBackupAgent\"//; s/android\:extractNativeLibs=\"false\"/android\:extractNativeLibs=\"true\"/' AndroidManifest.xml
gsed -i -e '/com.google.android.backup.api_key/d' AndroidManifest.xml

gsed -i -e '/LabelSecondary.9Key/d' res/layout/softkey_{zhuyin,cangjie}_label_character_label_header.xml

rm -rf res/values-zh-rCN/strings.xml

gsed -i -e '/variant_pcqwerty/a\
\ \ \ \ \ \ \ \ <item>sangaline</item>\
\ \ \ \ \ \ \ \ <item>@string/variant_sangaline</item>' res/values/arrays.xml

gsed -i -e '/"cloud_sync_enabled"/s/true/false/; /"enable_feature_cards"/s/false/true/; /"enable_joystick_delete"/s/false/true/' res/values/bools.xml
gsed -i -e '/"enable_m2_emoji_horizontal_scroll"/s/false/true/; /"enable_m2_for_gif_revamp"/s/false/true/; /"enable_m2_for_gif_revamp_default_to_recent"/s/false/true/; /"enable_m2_for_gif_telescoping_search"/s/false/true/; /"enable_m2_gif_horizontal_scroll"/s/false/true/; /"enable_m2_horizontal_scroll"/s/false/true/; /"enable_m2_search_box_trending_search_chips"/s/false/true/; /"enable_m2_tenor_autocomplete"/s/false/true/' res/values/bools.xml
gsed -i -e '/"enable_rate_us_feature_card"/s/true/false/; /"enable_rate_us_from_image_insert"/s/true/false/; /"enable_rate_us_in_settings"/s/true/false/; /"enable_rate_us_search_card"/s/true/false/' res/values/bools.xml
gsed -i -e '/"enable_search_corpus"/s/true/false/' res/values/bools.xml
gsed -i -e '/"firebase_enabled"/s/true/false/' res/values/bools.xml
gsed -i -e '/"pref_def_value_chinese_english_mixed_input_zh_tw"/s/true/false/' res/values/bools.xml
gsed -i -e '/"pref_def_value_enable_conv2query"/s/true/false/; /"pref_def_value_enable_conv2query_training"/s/true/false/' res/values/bools.xml
gsed -i -e '/"pref_def_value_enable_vibrate_on_keypress"/s/true/false/; /"pref_def_value_enable_voice_input"/s/true/false/' res/values/bools.xml
gsed -i -e '/"pref_def_value_import_user_contacts"/s/true/false/; /"pref_def_value_latin_enable_user_metrics"/s/true/false/' res/values/bools.xml

gsed -i -e '/"enable_new_user_feature_targeting"/s/false/true/' res/values/bools.xml
gsed -i -e '/"enable_settings_search"/s/false/true/' res/values/bools.xml

gsed -i -e '/"themed_nav_bar_style"/s/0/2/' res/values/integers.xml
gsed -i -e '/"variant_samoan"/a\
\ \ \ \ <string name="variant_sangaline">Sangaline</string>' res/values/strings.xml

gsed -i -e '/\/if>/i\
\ \ \ \ \ \ \ \ \ \ \ \ <keyboard_group variant="sangaline">\
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ <keyboard def="@xml/keyboard_fragment_en" type="prime">\
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ <merge def="@xml/keyboard_latin_sangaline" \/>\
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ <\/keyboard>\
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ <include href="@xml/keyboards_non_prime_latin" \/>\
\ \ \ \ \ \ \ \ \ \ \ \ <\/keyboard_group>' res/xml/ime_en_us.xml

gsed -i -e 's/\ language_specific_settings="@xml\/zh_tw_language_settings"//' res/xml/ime_zh_tw_zhuyin.xml
gsed -i -e '/keymapping_latin_fragment_secondary_digits/d' res/xml/keyboard_latin_fragment_superscript.xml
