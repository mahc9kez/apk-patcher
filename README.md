# APK Patcher

Patch APK and then upload to Telegram.

## Usage

Create a Telegram Bot by [@Botfather](https://t.me/botfather) and and get bot token from there, and then get `chat_id` from  
`https://api.telegram.org/bot${TG_BOT_TOKEN}/getUpdates`

Additional environment variables for GitLab CI, need to be encoded in BASE64, can be set as Masked Variables

```
CI_ANDROID_KEYSTORE
CI_ANDROID_STOREPASS
TG_BOT_TOKEN
TG_CHAT_ID
```

## Create a patch

```
apktool d app.apk && cd app
echo -e "/build/\n/dist/" > .gitignore
git init .
git add .
git commit -a -m "Initial commit"
# make any changes
apktool b ./ -o ../output.apk # zipalign, sign and test the apk
git commit -a -m "some changes" -m "some of other changes"
git format-patch -1 HEAD
```

Then add info to the patch, check existing patches as example

## Icon
it's a mix of [file_569829](https://www.flaticon.com/free-icon/files_569829) and [wrench_953761](https://www.flaticon.com/free-icon/wrench_953761)
